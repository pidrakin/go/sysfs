package sysfs

import (
	"fmt"
	"io"
	"os"
	"path"
)

func Copy(source, dest string, writer io.Writer) error {
	srcInfo, err := os.Stat(source)
	if err != nil {
		return err
	}
	var entries []os.FileInfo
	if srcInfo.IsDir() {
		if err = CreateIfNotExists(dest, srcInfo.Mode()); err != nil {
			return err
		}
		dirEntries, err := os.ReadDir(source)
		for _, entry := range dirEntries {
			info, err := entry.Info()
			if err != nil {
				return err
			}
			entries = append(entries, info)
		}
		if err != nil {
			return err
		}
	} else {
		if err := copyFile(source, dest); err != nil {
			return err
		}
		if err := postProcess(srcInfo, dest); err != nil {
			return err
		}
	}
	for _, entry := range entries {
		sourcePath := path.Join(source, entry.Name())
		destPath := path.Join(dest, entry.Name())

		switch entry.Mode() & os.ModeType {
		case os.ModeDir:
			if err := CreateIfNotExists(destPath, 0755); err != nil {
				return err
			}
			if err := Copy(sourcePath, destPath, writer); err != nil {
				return err
			}
		case os.ModeSymlink:
			if err := CopySymLink(sourcePath, destPath, writer); err != nil {
				return err
			}
		default:
			if err := copyFile(sourcePath, destPath); err != nil {
				return err
			}
		}

		if err := postProcess(entry, destPath); err != nil {
			return err
		}
	}
	return nil
}

func postProcess(entry os.FileInfo, destPath string) error {
	if err := postProcessChown(entry, destPath); err != nil {
		return err
	}

	isSymlink := entry.Mode()&os.ModeSymlink != 0
	if !isSymlink {
		if err := os.Chmod(destPath, entry.Mode()); err != nil {
			return err
		}
	}
	return nil
}

func copyFile(srcFile, dstFile string) error {
	out, err := os.Create(dstFile)
	if err != nil {
		return err
	}

	defer out.Close()

	in, err := os.Open(srcFile)
	defer in.Close()
	if err != nil {
		return err
	}

	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}

	return nil
}

func CreateIfNotExists(dir string, perm os.FileMode) error {
	if DirExists(dir) {
		return nil
	}

	if err := os.MkdirAll(dir, perm); err != nil {
		return fmt.Errorf("failed to create directory: '%s', error: '%s'", dir, err.Error())
	}

	return nil
}

func CopySymLink(source, dest string, writer io.Writer) error {
	link, err := ReadLink(source)
	if err != nil {
		return err
	}
	return Symlink(link, dest, writer)
}
