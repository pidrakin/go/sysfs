package sysfs

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

func ReadLink(name string) (target string, err error) {
	errorMsg := fmt.Sprintf("reading link [%s] failed: %%s", name)

	if target, err = os.Readlink(name); err != nil {
		if _, ok := err.(*os.PathError); ok {
			return "", fmt.Errorf(errorMsg, err.Error())
		}
	}
	if _, err = os.Stat(target); os.IsNotExist(err) {
		return "", fmt.Errorf(errorMsg, "link is dead")
	}
	return
}

func Symlink(source string, target string, writer io.Writer) (err error) {
	errorMsg := fmt.Sprintf("symlinking [%s] to [%s] failed: %%s", source, target)

	if !Exists(source) {
		return fmt.Errorf(errorMsg, "source does not exist")
	}

	targetDestination, _ := ReadLink(target)

	if source == targetDestination {
		return
	}
	if !DryRun {
		if targetDestination != "" {
			if err = os.Remove(target); err != nil {
				return fmt.Errorf(errorMsg, err.Error())
			}
		}
		if err = os.Symlink(source, target); err != nil {
			return fmt.Errorf(errorMsg, err.Error())
		}
	}
	return
}

func DeleteSymlink(linkRoot string, linkPath string) (err error) {
	if !DryRun {
		return os.Remove(filepath.Join(linkRoot, linkPath))
	}
	return
}
