package sysfs

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/pidrakin/go/tests"
	"os"
	"path/filepath"
	"testing"
)

func TestReadLink(t *testing.T) {
	t.Run("fail: link is dead", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		source := "random/path"
		target := "link"

		err := os.Symlink(source, target)
		tests.AssertNoError(t, err)

		retrieved, err := ReadLink(target)
		assert.Empty(t, retrieved)
		tests.AssertEqualError(t, err, "reading link [link] failed: link is dead")
	})

	t.Run("success", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		source := "random/path"
		target := "link"

		err := os.MkdirAll(filepath.Dir(source), os.FileMode(0755))
		tests.AssertNoError(t, err)

		_, err = os.Create(source)
		tests.AssertNoError(t, err)

		err = os.Symlink(source, target)
		tests.AssertNoError(t, err)

		retrieved, err := ReadLink(target)
		tests.AssertNoError(t, err)

		assert.Equal(t, source, retrieved)
	})
}

func TestSymlink(t *testing.T) {
	t.Run("fail: target does not exist", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		source := "random/path"
		target := "link"

		var buf bytes.Buffer
		err := Symlink(source, target, &buf)
		tests.AssertEqualError(t, err, "symlinking [random/path] to [link] failed: source does not exist")

		// TODO: assert buf
	})
	t.Run("success: symlink DryRun", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		DryRun = true
		defer func() {
			DryRun = false
		}()

		source := "random/path"
		target := "link"

		err := os.MkdirAll(filepath.Dir(source), os.FileMode(0755))
		tests.AssertNoError(t, err)

		_, err = os.Create(source)
		tests.AssertNoError(t, err)

		var buf bytes.Buffer
		err = Symlink(source, target, &buf)
		tests.AssertNoError(t, err)

		// TODO: assert buf
	})
	t.Run("success", func(t *testing.T) {
		defer tests.ChTmpDir(t)()

		source := "random/path"
		target := "link"

		err := os.MkdirAll(filepath.Dir(source), os.FileMode(0755))
		tests.AssertNoError(t, err)

		_, err = os.Create(source)
		tests.AssertNoError(t, err)

		var buf bytes.Buffer
		err = Symlink(source, target, &buf)
		tests.AssertNoError(t, err)

		// TODO: assert buf
	})
}
