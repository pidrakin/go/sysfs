//go:build darwin || linux

package sysfs

import (
	"os"
	"syscall"
)

func postProcessChown(entry os.FileInfo, destPath string) error {
	uid := 0
	gid := 0
	stat, ok := entry.Sys().(*syscall.Stat_t)
	if ok {
		uid = int(stat.Uid)
		gid = int(stat.Gid)
	}
	return os.Chown(destPath, uid, gid)
}
