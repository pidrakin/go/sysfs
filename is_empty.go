package sysfs

import (
	"fmt"
	"os"
)

// IsEmpty checks if a given file or directory is empty.
func IsEmpty(path string) (bool, error) {
	if !Exists(path) {
		return false, fmt.Errorf("[%s] path does not exist", path)
	}
	fi, err := os.Stat(path)
	if err != nil {
		return false, err
	}
	if fi.IsDir() {
		f, err := os.Open(path)
		if err != nil {
			return false, err
		}
		defer f.Close()
		list, err := f.Readdir(-1)
		return len(list) == 0, nil
	}
	return fi.Size() == 0, nil
}
