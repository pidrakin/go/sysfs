package sysfs

import (
	"os"
	"path/filepath"
)

func Exists(path string) bool {
	if !filepath.IsAbs(path) {
		wd, err := os.Getwd()
		if err != nil {
			return false
		}
		path = filepath.Join(wd, path)
	}
	_, err := os.Stat(path)
	return err == nil
}
